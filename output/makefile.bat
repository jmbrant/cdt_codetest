@ECHO OFF

REM clean .o files
del /f *.o

REM Compile C files
gcc -Wall -c ..\src\*.c

REM Compile our executable file
gcc -o ..\CodingTest *.o

REM Take us back to previous directory for nice command line compilation
cd ..