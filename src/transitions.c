/***************************************************************************
 *
 *	transitions.c
 *	Author - Jesse Brantley
 *
 *  Description:
 *
 *	This file contains any information, functions, and data regarding
 *  transitions. 
 * 
 **************************************************************************/

#include <stdint.h>
#include "transitions.h"
#include "packets.h"

/***************************************************************************
 * Global Variables
 **************************************************************************/
transition_t gCurrentTransition;
uint32_t gTransitionStartTime = 0;


/***************************************************************************
 * Local Prototypes
 **************************************************************************/
transition_thresholds_t getPacketThreshold(packet_t packet);
bool transitionIsValid(transition_thresholds_t old, transition_thresholds_t new);


/***************************************************************************
 * Functions
 **************************************************************************/

/***************************************************************************
 * Name: 
 * 		bool packetIsTransition(packet_t packet)
 * 
 * Parameters:
 * 		packet_t packet
 * 			Contains the packet to be validated in state machine
 * 
 * Return:
 * 		bool
 * 			Returns true if the packet progresses the state machine,
 * 			triggering a transition
 * 
 * Description:
 * 		Contains the state logic for transition and validates incoming
 * 		packets to not only drive the logic, but to trigger transition
 * 		prints to console 
 * 		
 **************************************************************************/ 
bool packetIsTransition(packet_t packet)
{
	static transition_thresholds_t state = TRANSITION_THRESHOLD_ZERO;
	static bool transitionStarted	 = false;
	transition_thresholds_t packetThreshold = getPacketThreshold(packet);

	//check if we are still in the same state, or a transitional state
	if((packetThreshold == state) ||
		(packetThreshold == TRANSITION_THRESHOLD_NOT_DEFINED))
	{
		//clear any active transitions
		transitionStarted = false;
		return false;
	}
	else
	{
		//check if this new state is a legal transition
		if(transitionIsValid(state, packetThreshold))
		{
			//check if this is the first packet of this transition
			if(!transitionStarted)
			{
				//start the transition timer
				gTransitionStartTime = packet.time;

				//set our transition start flag
				transitionStarted = true;

				//since transition is not official until a full 10ms this still isn't transition
				return false;
			}
			else
			{
				//has it been 10ms in this state?
				if((packet.time - gTransitionStartTime) >= TEN_MS)
				{
					//the transition is complete. All factors satisfied.

					//Reset flags and set new state
					transitionStarted = false;
					state = packetThreshold;
					return true;
				}
				return false;
			}
		}
		else
		{
			//if our new packet not legal, or not the same, or a transitional, powerstate 
			//throw big error as something may have went wrong!
			printf("ERR: Illegal transition detected! State not changed.\n");

			//Do not change state, ensure our transition flag is reset, and move on with processing packets
			transitionStarted = false;
			return false;
		}
	}
}

/***************************************************************************
 * Name: 
 * 		transition_thresholds_t getPacketThreshold(packet_t packet)
 * 
 * Parameters:
 * 		packet_t packet
 * 			Contains the packet to be evaluated
 * 
 * Return:
 * 		transition_thresholds_t
 * 			Returns the transition state that corresponds to the power 
 * 			contained in the packet
 * 
 * Description:
 * 		Finds which transition state the packet belongs in
 * 		
 **************************************************************************/
transition_thresholds_t getPacketThreshold(packet_t packet)
{
	uint64_t power = calculatePower(packet);

	//Packet is within state 0
	if((power >= STATE_ZERO_MIN_POWER) &&
		(power <= STATE_ZERO_MAX_POWER))
	{
		return TRANSITION_THRESHOLD_ZERO;
	}

	//Packet is within state 1
	if((power >= STATE_ONE_MIN_POWER) &&
		(power <= STATE_ONE_MAX_POWER))
	{
		return TRANSITION_THRESHOLD_ONE;
	}

	//Packet is within state 2
	if((power >= STATE_TWO_MIN_POWER) &&
		(power <= STATE_TWO_MAX_POWER))
	{
		return TRANSITION_THRESHOLD_TWO;
	}

	//Packet is within state 3
	if((power >= STATE_THREE_MIN_POWER) &&
		(power <= STATE_THREE_MAX_POWER))
	{
		return TRANSITION_THRESHOLD_THREE;
	}

	return TRANSITION_THRESHOLD_NOT_DEFINED;
}

/***************************************************************************
 * Name: 
 * 		bool transitionIsValid(transition_thresholds_t old, 
 * 					transition_thresholds_t new)
 * 
 * Parameters:
 * 		transition_thresholds_t old
 * 			Contains the existing state in the state machine
 * 		transition_thresholds_t new
 * 			Contains the new state from the packet
 * 
 * Return:
 * 		bool
 * 			Returns true if the transition is legal
 * 
 * Description:
 * 		Determines if a transition from old state to new one is legal,
 * 		determined from the transition diagram
 * 		
 **************************************************************************/
bool transitionIsValid(transition_thresholds_t old, transition_thresholds_t new)
{
	// 0 -> 1 starting
	if((old == TRANSITION_THRESHOLD_ZERO) && (new == TRANSITION_THRESHOLD_ONE))
	{
		gCurrentTransition = TRANSITION_STARTING;
		return true;
	}

	// 1 -> 2 warmup
	if((old == TRANSITION_THRESHOLD_ONE) && (new == TRANSITION_THRESHOLD_TWO))
	{
		gCurrentTransition = TRANSITION_WARM_UP;
		return true;
	}

	// 2 -> 3 main session
	if((old == TRANSITION_THRESHOLD_TWO) && (new == TRANSITION_THRESHOLD_THREE))
	{
		gCurrentTransition = TRANSITION_MAIN_SESSION;
		return true;
	}

	// 3 -> 2 cool down
	if((old == TRANSITION_THRESHOLD_THREE) && (new == TRANSITION_THRESHOLD_TWO))
	{
		gCurrentTransition = TRANSITION_COOL_DOWN;
		return true;
	}

	// 2 -> 0 
	if((old == TRANSITION_THRESHOLD_TWO) && (new == TRANSITION_THRESHOLD_ZERO))
	{
		gCurrentTransition = TRANSITION_COMPLETE;
		return true;
	}

	return false;
}
