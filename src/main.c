/***************************************************************************
 *
 *	main.c - Program Main
 *	Author - Jesse Brantley
 *
 *  Description:
 *
 *	This program is to be an example program for CDT
 *
 *	Basic functionality is to read an input stream of data (from example file)
 *	and interpret the data within set parameters of given packet format.
 * 
 **************************************************************************/

#include <stdio.h>
#include <stdint.h>

#include "packets.h"
#include "transitions.h"


/***************************************************************************
 * Functions
 **************************************************************************/

/***************************************************************************
 * Name: 
 * 		int main(int argc, char *argv[])
 * 
 * Parameters:
 * 		int argc
 * 			Arguments count
 * 		char *argv[]
 * 			Arguments
 * 
 * Return:
 * 		unused
 * 
 * Description:
 * 		Main function. Called on program initialization. Runs to completion.
 * 
 **************************************************************************/
int main(int argc, char *argv[])
{
	FILE *fp;
	packet_t packet;
	packet_error_t err;

	//Parameter checking
	if( argc == 2 ) {
	}
	else if( argc > 2 ) {
		printf("ERR: Too many arguments supplied.\n");
	}
	else {
		printf("ERR: One argument expected.\n");
	}

	//open our read file
	fp = fopen(argv[1], "rb");

	//Grab our initial packet 
	err = getNextPacket(fp, &packet);

	//error checking
	if(err == PACKET_ERROR_EOF)
	{
		printf("ERR: File does not exist\n");
		return 0;
	}
	else if(err == PACKET_ERROR_BAD_CHECKSUM)
	{
		printf("ERR: Bad Checksum\n");
	}
	else if(err == PACKET_ERROR_UNKNOWN_TYPE)
	{
		printf("ERR: Unknown Packet Type\n");
	}

	//take action on packet
	processValidPacket(packet);

	do
	{
		//Grab our next packet from the file
		err = getNextPacket(fp, &packet);

		//error check
		if(err == PACKET_ERROR_EOF)
		{
			//if EOF, break out of this do-while
			break;
		}
		else if(err == PACKET_ERROR_BAD_CHECKSUM)
		{
			printf("ERR: Bad Checksum\n");
		}
		else if(err == PACKET_ERROR_UNKNOWN_TYPE)
		{
			printf("ERR: Unknown Packet Type\n");
		}

		//take action on packet
		processValidPacket(packet);
	}while(err != PACKET_ERROR_EOF);

	//end of program
}
