#ifndef PACKETS_H
#define PACKETS_H

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

/***************************************************************************
 * Enums
 **************************************************************************/
typedef enum packet_type
{
	PACKET_TYPE_DATA_VOLTAGE,
	PACKET_TYPE_BATTERY
}packet_type_t;

typedef enum batt_status
{
	BATTERY_VERY_LOW 	= 0x00,
	BATTERY_LOW 		= 0x01,
	BATTERY_MEDIUM 		= 0x02,
	BATTERY_HIGH 		= 0x03
}batt_status_t;

typedef enum packet_error
{
	PACKET_OK,
	PACKET_ERROR_EOF,
	PACKET_ERROR_BAD_CHECKSUM,
	PACKET_ERROR_UNKNOWN_TYPE
}packet_error_t;


/***************************************************************************
 * Structs
 **************************************************************************/
typedef struct packet
{
	packet_type_t 	type;
	uint32_t 		time;
	uint32_t 		voltage;
	uint64_t		current;
	batt_status_t	batteryStatus;
	uint8_t   		errorCheck;
}packet_t;


/***************************************************************************
 * Defines
 **************************************************************************/
#define DATA_VOLTAGE_BYTE		0x00
#define BATTERY_BYTE			0x01

#define NUM_U8_BITS				8U
#define NUM_U16_BITS			16U
#define NUM_U32_BITS			32U
#define NUM_U64_BITS			64U


/***************************************************************************
 * Global Function Prototypes
 **************************************************************************/
packet_error_t getNextPacket(FILE *fp, packet_t *packet);
bool packetIsValid(packet_t packet);
uint64_t calculatePower(packet_t packet);
void processValidPacket(packet_t packet);
void printPacket(packet_t packet);
void printPacketHex(packet_t packet);

#endif
