/***************************************************************************
 *
 *	packets.c
 *	Author - Jesse Brantley
 *
 *  Description:
 *
 *	This file contains functions and data related to packets. How they
 *  are designed and any processing around them is handled here.
 * 
 **************************************************************************/

#include <stdio.h>
#include <stdint.h>

#include "packets.h"
#include "transitions.h"


/***************************************************************************
 * External Variables
 **************************************************************************/
extern transition_t gCurrentTransition;
extern uint32_t gTransitionStartTime;

/***************************************************************************
 * Local Prototypes
 **************************************************************************/
uint32_t getSecondsfromMS(uint32_t time);


/***************************************************************************
 * Functions
 **************************************************************************/

/***************************************************************************
 * Name: 
 * 		packet_error_t getNextPacket(FILE *fp, packet_t *packet)
 * 
 * Parameters:
 * 		FILE *fp
 * 			Pointer to the readfile containing the packets
 * 		packet_t *packet
 * 			Return variable for the packet data
 * 
 * Return:
 * 		packet_error_t
 * 			Error type
 * 
 * Description:
 * 		Parses the next data packet from the provided file
 * 		
 **************************************************************************/
packet_error_t getNextPacket(FILE *fp, packet_t *packet)
{
	packet_error_t err = PACKET_OK;
	// ---- GET PACKET TYPE ----
	//grab a single character, defining the packet type
	int type_character = fgetc(fp);

	//check for packet type
	if((uint8_t)type_character == DATA_VOLTAGE_BYTE)
	{
		packet->type = PACKET_TYPE_DATA_VOLTAGE;
	}
	else if((uint8_t)type_character == BATTERY_BYTE)
	{
		packet->type = PACKET_TYPE_BATTERY;
	}
	else if(type_character == EOF)
	{
		return PACKET_ERROR_EOF;
	}
	else
	{
		err = PACKET_ERROR_UNKNOWN_TYPE;
	}


	// ---- GET TIME ----
	packet->time = 
		((uint32_t) (fgetc(fp) & 0xFF) << 24) |
		((uint32_t) (fgetc(fp) & 0xFF) << 16) |
		((uint32_t) (fgetc(fp) & 0xFF) <<  8) |
		((uint32_t) (fgetc(fp) & 0xFF) <<  0) ;

	//Process packet payload (payload changed depending on packet type)
	if(packet->type == PACKET_TYPE_DATA_VOLTAGE)
	{
		// ---- GET VOLTAGE ----
		packet->voltage = 
			((uint32_t) (fgetc(fp) & 0xFF) << 24) |
			((uint32_t) (fgetc(fp) & 0xFF) << 16) |
			((uint32_t) (fgetc(fp) & 0xFF) <<  8) |
			((uint32_t) (fgetc(fp) & 0xFF) <<  0) ;

		// ---- GET CURRENT ----
		packet->current = 
			((uint64_t) (fgetc(fp) & 0xFF) << 56) |
			((uint64_t) (fgetc(fp) & 0xFF) << 48) |
			((uint64_t) (fgetc(fp) & 0xFF) << 40) |
			((uint64_t) (fgetc(fp) & 0xFF) << 32) |
			((uint64_t) (fgetc(fp) & 0xFF) << 24) |
			((uint64_t) (fgetc(fp) & 0xFF) << 16) |
			((uint64_t) (fgetc(fp) & 0xFF) <<  8) |
			((uint64_t) (fgetc(fp) & 0xFF) <<  0) ;
	}
	else //packet must == battery
	{
		// ---- GET BATT STATUS ----
		packet->batteryStatus = (uint8_t) fgetc(fp);

		//error checking
		if(packet->batteryStatus > BATTERY_HIGH)
		{
			printf("ERR: Illegal Battery Status Value.\n");
		}
	}


	// ---- GET ERROR BYTE ----
	packet->errorCheck = (uint8_t) fgetc(fp);

	//Check for if packet is valid
	if(!packetIsValid(*packet))
	{
		err = PACKET_ERROR_BAD_CHECKSUM;
	}

	return err;
}

/***************************************************************************
 * Name: 
 * 		bool packetIsValid(packet_t packet)
 * 
 * Parameters:
 * 		packet_t packet
 * 			Contains packet to check
 * 		
 * 
 * Return:
 * 		bool
 * 			True or false on if packet checksum is validated
 * 		
 * 
 * Description:
 * 		Checks if a packets checksum is valid
 * 		
 **************************************************************************/
bool packetIsValid(packet_t packet)
{
	uint8_t sum = 0;

	//Add TIME byte values to our sum
	for(int i = 0; i < NUM_U32_BITS; i += 8)
	{
		sum += (uint8_t)((packet.time >> i) & 0xFF);
	}

	//calculations based on packet type
	if(packet.type == PACKET_TYPE_BATTERY)
	{
		//Add in Packet type byte
		sum += 1;

		//Add Battery status bytes to the sum
		sum += packet.batteryStatus;
	}
	else
	{	
		//Add in bytes for Voltage
		for(int i = 0; i < NUM_U32_BITS; i += 8)
		{
			sum += (uint8_t)((packet.voltage >> i) & 0xFF);
		}

		//Add in bytes for Current
		for(int i = 0; i < NUM_U64_BITS; i += 8)
		{
			sum += (uint8_t)((packet.current >> i) & 0xFF);
		}

	}

	//Does our calculated sum match the provided error check byte?
	if(sum == packet.errorCheck)
	{
		return true;
	}

	return false;
}

/***************************************************************************
 * Name: 
 * 		uint64_t calculatePower(packet_t packet)
 * 
 * Parameters:
 * 		packet_t packet
 * 			Packet that you wish to calculate the power draw in
 * 
 * Return:
 * 		uint64_t
 * 			Power in mA
 * 
 * Description:
 * 		Calculates the power from the packets current and voltage
 * 
 **************************************************************************/
uint64_t calculatePower(packet_t packet)
{
	//P = I*R
	return packet.current * packet.voltage;
}

/***************************************************************************
 * Name: 
 * 		void printPacket(packet_t packet)
 * 
 * Parameters:
 * 		packet_t packet
 * 			Packet you wish to print
 * 
 * Return:
 * 		none 		
 * 
 * Description:
 * 		Prints the provided packet to the command line
 * 
 **************************************************************************/
void printPacket(packet_t packet)
{
	if(packet.type == PACKET_TYPE_DATA_VOLTAGE)
	{
		//print packet type
		printf("S;");

		//print seconds
		printf("%u;", getSecondsfromMS(gTransitionStartTime));

		//print transition status
		switch(gCurrentTransition)
		{
			case TRANSITION_STARTING:
			  printf("0-1\n");
			  break;
			case TRANSITION_WARM_UP:
			  printf("1-2\n");
			  break;
			case TRANSITION_MAIN_SESSION:
			  printf("2-3\n");
			  break;
			case TRANSITION_COOL_DOWN:
			  printf("3-2\n");
			  break;
			case TRANSITION_COMPLETE:
			  printf("2-0\n");
			  break;
		}
	}
	else if(packet.type == PACKET_TYPE_BATTERY)
	{
		//print packet type
		printf("B;");

		//print seconds
		printf("%u;", getSecondsfromMS(packet.time));

		//print battery status
		switch(packet.batteryStatus)
		{
			case BATTERY_VERY_LOW:
			  printf("VLOW\n");
			  break;
			case BATTERY_LOW:
			  printf("LOW\n");
			  break;
			case BATTERY_MEDIUM:
			  printf("MED\n");
			  break;
			case BATTERY_HIGH:
			  printf("HIGH\n");
			  break;
		}
	}
}

/***************************************************************************
 * Name: 
 * 		void printPacketHex(packet_t packet)
 * 
 * Parameters:
 * 		packet_t packet
 * 			Contains the packet you wish to print
 * 
 * Return:
 * 		none
 * 
 * Description:
 * 		Prints a hex readable output of the entire packet.
 * 		USED FOR TESTING
 * 
 **************************************************************************/
void printPacketHex(packet_t packet)
{
	if(packet.type == PACKET_TYPE_DATA_VOLTAGE)
	{
		printf("Data Voltage Packet  = 0x00\r\n");
		printf("    Time             = %#04x\r\n", packet.time);
		printf("    Voltage          = %#04x\r\n", packet.voltage);
		printf("    Current          = %#I64x\r\n", packet.current);
		printf("    ErrorCheck       = %#01x\r\n", packet.errorCheck);
	}
	else if(packet.type == PACKET_TYPE_BATTERY)
	{
		printf("Battery Status Packet = 0x01\r\n");
		printf("    Time              = %#04x\r\n", packet.time);
		printf("    Battery Status    = %#01x\r\n", packet.batteryStatus);
		printf("    Error Check       = %#01x\r\n", packet.errorCheck);
	}
}

/***************************************************************************
 * Name: 
 * 		uint32_t getSecondsfromMS(uint32_t time)
 * 
 * Parameters:
 * 		uint32_t time
 * 			Contains a time value in ms
 * 
 * Return:
 * 		uint32_t
 * 			Contains the seconds value, rounded down
 * 
 * Description:
 * 		Calculates the floor value in seconds of the ms value provided
 * 
 **************************************************************************/
uint32_t getSecondsfromMS(uint32_t time)
{
	//divide by 1000 to go from ms -> s (rounding down)
	return time/1000;
}

/***************************************************************************
 * Name: 
 * 		void processValidPacket(packet_t packet)
 * 
 * Parameters:
 * 		packet_t packet
 * 			Contains the packet that requires final action
 * 
 * Return:
 * 		none
 * 
 * Description:
 * 		Any actions required for a packet, once validated action is
 * 		necessary, are placed here. 
 * 
 **************************************************************************/
void processValidPacket(packet_t packet)
{
	//For any DV packets that aren't transitions, do nothing
	if((packet.type == PACKET_TYPE_DATA_VOLTAGE) &&
		(!packetIsTransition(packet)))
	{
		return;
	}

	//if packet requires printing, print it
	printPacket(packet);
}
