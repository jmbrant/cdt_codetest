#ifndef TRANSITIONS_H
#define TRANSITIONS_H

#include <stdbool.h>
#include "packets.h"

/***************************************************************************
 * Enums
 **************************************************************************/
typedef enum transition_state
{
	TRANSITION_STARTING,
	TRANSITION_WARM_UP,
	TRANSITION_MAIN_SESSION,
	TRANSITION_COOL_DOWN,
	TRANSITION_COMPLETE
} transition_t;

typedef enum transition_error
{
	TRANSITION_OK,
	TRANSITION_ERROR_ILLEGAL_TRANSITION
}transition_error_t;

typedef enum transition_thresholds
{
	TRANSITION_THRESHOLD_ZERO,
	TRANSITION_THRESHOLD_ONE,
	TRANSITION_THRESHOLD_TWO,
	TRANSITION_THRESHOLD_THREE,
	TRANSITION_THRESHOLD_NOT_DEFINED
}transition_thresholds_t;



/***************************************************************************
 * Defines
 **************************************************************************/
#define STATE_ZERO_MIN_POWER			0U
#define STATE_ZERO_MAX_POWER			200U

#define STATE_ONE_MIN_POWER				300U
#define STATE_ONE_MAX_POWER				450U

#define STATE_TWO_MIN_POWER				550U
#define STATE_TWO_MAX_POWER				650U

#define STATE_THREE_MIN_POWER			800U
#define STATE_THREE_MAX_POWER			1200U


#define TEN_MS							10U


/***************************************************************************
 * Global Function Prototypes
 **************************************************************************/
bool packetIsTransition(packet_t packet);

#endif
